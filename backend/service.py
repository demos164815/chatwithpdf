import subprocess

while True:
    process = subprocess.Popen(["python", "server.py"], stdout=subprocess.PIPE)
    output, unused_err = process.communicate()
    retcode = process.poll()
    if retcode == 3:  # Restart signal
        continue
    else:
        break