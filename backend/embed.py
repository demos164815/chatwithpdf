import os
import json
import glob

from langchain.document_loaders import PyPDFLoader

from langchain.embeddings.openai import OpenAIEmbeddings
from langchain.text_splitter import RecursiveCharacterTextSplitter
from langchain.vectorstores import Chroma

from dotenv import load_dotenv
load_dotenv()

chromadb = Chroma(persist_directory="./chroma")
for collection in chromadb._client.list_collections():
  ids = collection.get()['ids']
  print('REMOVE %s document(s) from %s collection' % (str(len(ids)), collection.name))
  if len(ids): collection.delete(ids)

pdf_files = glob.glob("data/*.pdf")
newpath=""
for path in pdf_files:
	newpath=path
	break
loader = PyPDFLoader(newpath)
data = loader.load()

text_splitter = RecursiveCharacterTextSplitter(
		chunk_size=1000,
		chunk_overlap=200,
)
documents = text_splitter.split_documents(data)
embedding_model = OpenAIEmbeddings(model="text-embedding-ada-002")
db = Chroma.from_documents(
	documents,
	embedding_model,
	persist_directory="./chroma"
)
db.persist()