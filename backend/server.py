from flask import Flask, jsonify, request
from flask_cors import CORS
from chat import get_response
from werkzeug.utils import secure_filename
import os
import glob
import subprocess

app = Flask(__name__)
CORS(app)

@app.route('/api/data', methods=['POST'])
def post_data():
    data = request.get_json()
    question = data['question']
    if(question.lower() == "hi" or question.lower() == "hello" or question.lower() == "hey"):
        return jsonify({'answer': 'Hello! How can I help you today?'})
    response = get_response(question)
    return response

@app.route('/api/upload', methods=['POST'])
def upload_file():
    d = {}
    try:
        target = os.path.join(os.getcwd(), 'data')
        if not os.path.isdir(target):
            os.mkdir(target)
        else:
            files = glob.glob(os.path.join(target, '*'))
            for f in files:
                os.remove(f)
        file = request.files['file'] 
        filename = secure_filename(file.filename)
        destination="/".join([target, filename])
        file.save(destination)
        subprocess.run(["python", "embed.py"])
        d['status'] = "Uploaded and processed file successfully"

    except Exception as e:
        print(f"Couldn't upload file {e}")
        d['status'] = "Failed to upload file"

    return jsonify(d)
@app.route('/api/alive', methods=['GET'])
def alive():
    return jsonify({'status': 'alive'})
@app.route('/api/restart', methods=['GET'])
def restart():
    print("Restarting...")
    os._exit(3) 
if __name__ == '__main__':
    #app.run(host='127.0.0.1',port=5000) uncomment this when running in local
    app.run()#comment it before running in local