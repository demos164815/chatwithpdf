import "./App.css";
import Chatbot from "./Chatbot";

function App() {
  return (
    <div>
      <h1 className="heading">AI Chatbot</h1>
      <Chatbot />
    </div>
  );
}

export default App;
