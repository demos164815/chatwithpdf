import React, { useState, useRef, useEffect } from "react";
import axios from "axios";
import "./Chatbot.css";

const Chatbot = () => {
  const [input, setInput] = useState("");
  const [messages, setMessages] = useState([]);
  const [isWaitingForResponse, setIsWaitingForResponse] = useState(false);
  const messagesEndRef = useRef(null);
  const [isFileUpload, setIsFileUpload] = useState(false);
  const [file, setFile] = useState(null);
  const [inputKey, setInputKey] = useState(Date.now());

  const scrollToBottom = () => {
    messagesEndRef.current?.scrollIntoView({ behavior: "smooth" });
  };

  useEffect(scrollToBottom, [messages]);

  const clearMessages = () => {
    setMessages([]);
  };

  const chatWithLLM = async (userInput) => {
    const apiEndpoint = "http://127.0.0.1:5000/api/data";
    const headers = {
      "Content-Type": "application/json",
    };

    const data = {
      question: userInput,
    };
    try {
      console.log("API request:", data);
      const response = await axios.post(apiEndpoint, data, { headers });
      console.log("API response:", response.data);
      return response.data.answer.trim();
    } catch (error) {
      console.error("Error communicating with the API:", error.message);
      return "";
    }
  };
  const restartServer = async () => {
    const apiEndpoint = "http://127.0.0.1:5000/api/restart";
    try {
      const response = await axios.get(apiEndpoint);
      console.log("API response:", response.data);
    } catch (error) {}
  };
  const uploadFile = async (file) => {
    const apiEndpoint = "http://127.0.0.1:5000/api/upload";
    const formData = new FormData();
    formData.append("file", file);
    const headers = {
      "Content-Type": "multipart/form-data",
    };
    try {
      console.log("API request:", formData);
      const response = await axios.post(apiEndpoint, formData, { headers });
      console.log("API response:", response.data);
      return response.data.status;
    } catch (error) {
      console.error("Error communicating with the API:", error.message);
      return "";
    }
  };
  const handleSubmit = async (e) => {
    e.preventDefault();
    if (!input.trim()) return;
    const userMessage = { text: input, user: true };
    var userInput = input.trim();
    setInput("");
    setMessages((prevMessages) => [...prevMessages, userMessage]);
    setIsWaitingForResponse(true);
    const aiMessage = { text: "...", user: false };
    setMessages((prevMessages) => [...prevMessages, aiMessage]);
    const response = await chatWithLLM(userInput);
    const newAiMessage = { text: response, user: false };
    setMessages((prevMessages) => [...prevMessages.slice(0, -1), newAiMessage]);

    setIsWaitingForResponse(false);
  };
  const handleFileSubmit = async (e) => {
    e.preventDefault();
    if (!file) return;
    setIsWaitingForResponse(true);
    const userMessage = { text: file.name, user: true };
    setMessages((prevMessages) => [...prevMessages, userMessage]);
    if (file && file.type === "application/pdf") {
      const aiMessage = { text: "Uploading and processing...", user: false };
      setMessages((prevMessages) => [...prevMessages, aiMessage]);
      var userFile = file;
      setFile(null);
      setInput("");
      setInputKey(Date.now());
      const response = await uploadFile(userFile);
      const newAiMessage = { text: response, user: false };
      setMessages((prevMessages) => [
        ...prevMessages.slice(0, -1),
        newAiMessage,
      ]);
      restartServer();
      const restartAiMessage = { text: "Loading the data...", user: false };
      setMessages((prevMessages) => [...prevMessages, restartAiMessage]);
      let serverIsUp = false;
      while (!serverIsUp) {
        try {
          await new Promise((resolve) => setTimeout(resolve, 5000));
          const serverResponse = await fetch("http://127.0.0.1:5000/api/alive");
          if (serverResponse.ok) {
            serverIsUp = true;
            const serverAliveMessage = { text: "Data Loaded", user: false };
            setMessages((prevMessages) => [
              ...prevMessages.slice(0, -1),
              serverAliveMessage,
            ]);
          }
        } catch (error) {
          // The fetch call will throw an error if the server is not up
          // Ignore the error and try again
        }
      }
    } else {
      const aiMessage = { text: "Please upload a PDF file", user: false };
      setMessages((prevMessages) => [...prevMessages, aiMessage]);
      setFile(null);
      setInput("");
      setInputKey(Date.now());
    }
    setIsWaitingForResponse(false);
  };
  return (
    <div className="chatbot-container">
      <div className="chatbot-messages">
        {messages.map((message, index) => (
          <div
            className={`message-unit ${
              message.user ? "user-message-unit" : "ai-message-unit"
            }`}
          >
            {message.user === false && <div className="avatar-ai">AI</div>}
            <div
              key={index}
              className={`message ${
                message.user ? "user-message" : "ai-message"
              }`}
            >
              {message.text}
            </div>
            {message.user === true && <div className="avatar-user">Me</div>}
          </div>
        ))}
        <div ref={messagesEndRef} />
      </div>
      <button
        type="button"
        className="switchButton"
        onClick={() => setIsFileUpload(!isFileUpload)}
      >
        Switch to {isFileUpload ? "Text Input" : "File Upload"}
      </button>
      <button className="clearButton" onClick={clearMessages}>
        Clear
      </button>
      {!isFileUpload && (
        <form className="chatbot-input-form" onSubmit={handleSubmit}>
          <input
            type="text"
            value={input}
            onChange={(e) => setInput(e.target.value)}
            disabled={isWaitingForResponse}
            placeholder={
              isWaitingForResponse ? "Please wait..." : "Type your message..."
            }
          />
          <button type="submit">Send</button>
        </form>
      )}
      {isFileUpload && (
        <form className="chatbot-input-form" onSubmit={handleFileSubmit}>
          <input
            key={inputKey}
            type="file"
            accept="application/pdf"
            disabled={isWaitingForResponse}
            onChange={(e) => {
              const file = e.target.files[0];
              setFile(file);
              const reader = new FileReader();
              reader.onload = async (e) => {
                const text = e.target.result;
                setInput(text);
              };
              reader.readAsText(file);
            }}
          />
          <button type="submit">Upload</button>
        </form>
      )}
    </div>
  );
};
export default Chatbot;
