# chatwithpdf
![alt text](chatwithpdf.png)

## Description
This project is a full-stack web application developed using React and Python that integrates a Language Learning Model (LLM) for question answering based on a specific PDF document. Users can ask questions related to the provided PDF, and the application utilizes RAG techniques for accurate answers.


## Installation
To set up the project locally, follow these steps:

1. Clone the repository:
   ```sh
   git clone <repository_url>
   ```

2. Navigate to the project directory:
    ```sh
    cd chatwithpdf
    ```

3. Navigate to the frontend directoary and install frontend dependencies:

    ```sh
    cd frontend
    ```

    ```sh
    npm install
    ```
    Switch back to main directory,

    ```sh
    cd..
    ```
4. Install backend dependencies (assuming Python and pip are already installed):

    Go to backend folder 

    ```sh
    cd backend
    ```

    ```sh
    pip install -r requirements.txt
    ```
    If you encounter error: Microsoft Visual C++ 14.0 or greater is required. Get it with "Microsoft C++ Build Tools": https://visualstudio.microsoft.com/visual-cpp-build-tools/. Download it from given site and install as instructed in this screenshot. Then rerun the above install command.
    ![alt text](VS.png)

5. Set up OpenAI API key:
    (chatwithpdf->backend->.env)
    Obtain an API key from OpenAI and set it as an environment variable:
        
    ```sh
    OPENAI_API_KEY=<your_api_key>
    ```

6. By default the app server will run on 127.0.0.1 host and 5000 port number. If you wish to change this
   configuration please modify server.py and Chatbot.js accordingly.

## Usage

1. Start the backend service:
    Go to backend folder 

    ```sh
    cd backend
    ```

    ```sh
    python service.py
    ```

2. Start the frontend development server:
    From a new cmd, 
    Go to frontend folder
    
    ```sh
    cd frontend
    ```

    ```sh
    npm start
    ```

Open your web browser and navigate to http://localhost:3000 to view the application.
![alt text](ui.png)

Ask questions related to the provided PDF document or upload your own PDF for question answering.
View the answers provided by the LLM and engage in conversation.

## Architecture
The application follows a client-server architecture:

The frontend is built using React, providing a user-friendly interface for asking questions.
The backend is developed using Python with Flask, providing endpoints for communication with the frontend and PDF processing. This application uses ConversationBufferMemory to remember previous chat. 
File upload feature requires backend server restart due to limitations in handling file uploads dynamically. This limitation is addressed by triggering a server restart from the frontend upon file upload.

## Technologies Used
React
Python

## Future Scope
Chatting with data from multiple PDF files.

## Contributing
Contributions are welcome! Please fork the repository, make your changes, and submit a pull request.